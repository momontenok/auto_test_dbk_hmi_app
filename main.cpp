#include <fstream>
#include "libs/json.hpp"
using json = nlohmann::json;

#include <iostream>
#include <ncurses.h>
#include <sys/socket.h>
#include <linux/can/raw.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <unistd.h>

std::ifstream f("test.json");
json data = json::parse(f);

void gotoxy(int x, int y) {
    move(y, x);
}
int speedometer_v2 = data["CatalogTestHarvesterScreen"]["speedometer"]["v2"];
void showMenu(int activeMenuItem, int activeSubMenuItem, bool showSubmenu, int selectedCatalog) {
    //clear();

    printw("CatalogTestHarvesterScreen \n", data["CatalogTestHarvesterScreen"]);
    if (selectedCatalog == 0) {
        activeSubMenuItem = activeMenuItem + 1;
        gotoxy(0, selectedCatalog + 1);
        if (activeSubMenuItem == 1){
            attron(A_REVERSE);
            printw("   Speedometer \n", data["CatalogTestHarvesterScreen"]["speedometer"]);
            attroff(A_REVERSE);
            printw("   Tachometer \n", data["CatalogTestHarvesterScreen"]["tachometer"]);
        } 
        else if (activeSubMenuItem == 2) {
            printw("   Speedometer \n", data["CatalogTestHarvesterScreen"]["speedometer"]);
            attron(A_REVERSE);
            printw("   Tachometer \n", data["CatalogTestHarvesterScreen"]["tachometer"]);
            attroff(A_REVERSE);
        }   
    }
    
    printw("CatalogTestDriveScreen \n", data["CatalogTestDriveScreen"]);
    if (selectedCatalog == 1) {
        activeSubMenuItem = activeMenuItem + 1;
        gotoxy(0, selectedCatalog + 1);
        if (activeSubMenuItem == 1){
            attron(A_REVERSE);
            printw("   Speedometer \n", data["CatalogTestDriveScreen"]["speedometer"]);
            attroff(A_REVERSE);
            printw("   Tachometer \n", data["CatalogTestDriveScreen"]["tachometer"]);
        } 
        else if (activeSubMenuItem == 2) {
            printw("   Speedometer \n", data["CatalogTestDriveScreen"]["speedometer"]);
            attron(A_REVERSE);
            printw("   Tachometer \n", data["CatalogTestDriveScreen"]["tachometer"]);
            attroff(A_REVERSE);
        } 
    }
    
    printw("CatalogTestDiagnosticScreen \n", data["CatalogTestDiagnosticScreen"]);
    if (selectedCatalog == 2) {
        activeSubMenuItem = activeMenuItem + 1;
        gotoxy(0, selectedCatalog + 1);
        if (activeSubMenuItem == 1) {
            attron(A_REVERSE);
            printw("   ReelSpeed \n", data["CatalogTestDiagnosticScreen"]["ReelSpeed"]);
            attroff(A_REVERSE);
            printw("   EngTemp \n", data["CatalogTestDiagnosticScreen"]["EngTemp"]);
            printw("   Bunker \n", data["CatalogTestDiagnosticScreen"]["Bunker"]);
        } else if (activeSubMenuItem == 2) {
            printw("   ReelSpeed \n", data["CatalogTestDiagnosticScreen"]["ReelSpeed"]);
            attron(A_REVERSE);
            printw("   EngTemp \n", data["CatalogTestDiagnosticScreen"]["EngTemp"]);
            attroff(A_REVERSE);
            printw("   Bunker \n", data["CatalogTestDiagnosticScreen"]["Bunker"]);
        } else if (activeSubMenuItem == 3) {
            printw("   ReelSpeed \n", data["CatalogTestDiagnosticScreen"]["ReelSpeed"]);
            printw("   EngTemp \n", data["CatalogTestDiagnosticScreen"]["EngTemp"]);
            attron(A_REVERSE);
            printw("   Bunker \n", data["CatalogTestDiagnosticScreen"]["Bunker"]);
            attroff(A_REVERSE);
        }
    }
    //refresh();
}

int main() {
    std::ifstream f("test.json");
    json data = json::parse(f);
    std::cout << data.dump() << std::endl;

    setlocale(LC_ALL, "Russian");

    initscr(); 
    keypad(stdscr, TRUE);
    //noecho();

    int sock;
    if ((sock = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
        perror("Socket");
        return 1;
    }

    struct ifreq ifr;
    strcpy(ifr.ifr_name, "vcan0" );
    ioctl(sock, SIOCGIFINDEX, &ifr);

    struct can_frame frame;
    frame.can_id = 0x555;
    frame.can_dlc = 4;

    frame.data[0] = data["CatalogTestHarvesterScreen"]["speedometer"]["v1"];
    frame.data[1] = speedometer_v2;
    frame.data[2] = data["CatalogTestHarvesterScreen"]["speedometer"]["v3"];
    frame.data[3] = data["CatalogTestHarvesterScreen"]["speedometer"]["v4"];

    if (write(sock, &frame, sizeof(struct can_frame)) != sizeof(struct can_frame)) {
    perror("Write");
    return 1;
}
    else {
        std::cout << "Success" << std::endl;
    }
    
    const int NUM_MENU_ITEMS = 3;
    const int NUM_SUB_MENU_ITEMS = 2;
    int activeMenuItem = 0;
    int activeSubMenuItem = 0;
    int ch = 0;
    bool exit = false;
    bool showSubmenu = false;
    int selectedCatalog = -1;
    int memPoint;

    while (!exit) {
        showMenu(activeMenuItem, activeSubMenuItem, showSubmenu, selectedCatalog);
        gotoxy(0, activeMenuItem);

        ch = getch();

        switch (ch) {
            case 27: 
                if (selectedCatalog != -1) {
                    selectedCatalog = -1;
                    activeMenuItem = 0;
                    showSubmenu = false;
                } else {
                    exit = true;
                }
                break;
            case KEY_UP: 
                activeMenuItem--; 
                break;
            case KEY_DOWN: 
                activeMenuItem++; 
                break;
            case KEY_RIGHT:
                memPoint = activeMenuItem;
                if (selectedCatalog == -1) {
                    selectedCatalog = activeMenuItem;
                    activeMenuItem = 0;
                    showSubmenu = true;
                }
                switch (ch)
                {
                    case KEY_UP: 
                        activeSubMenuItem--; 
                        break;
                    case KEY_DOWN: 
                        activeSubMenuItem++; 
                        break;
                }
                break;
            case KEY_LEFT:
                if (selectedCatalog != -1) {
                    selectedCatalog = -1;
                    activeMenuItem = memPoint;
                    showSubmenu = false;
                }
                break;
            case 32:
                printw("Entered SPACE\n");
                if (selectedCatalog != -1 && showSubmenu) {
                switch (selectedCatalog) {
                    case 0:
                        if (activeSubMenuItem == 0) {
                            printw("Selected Speedometer in CatalogTestHarvesterScreen\n");
                        } else if (activeSubMenuItem == 1) {
                            printw("Selected Tachometer in CatalogTestHarvesterScreen\n");
                        }
                        break;
                    case 1:
                        
                        break;
                    case 2:
                        
                        break;
                }
            }
            else {
                printw("SLOMALOS'\n");
                }
            break;
        }
        if (activeMenuItem < 0) 
            activeMenuItem = NUM_MENU_ITEMS - 1;
        if (activeMenuItem >= NUM_MENU_ITEMS) 
            activeMenuItem = 0;

        if (activeSubMenuItem < 0) 
            activeSubMenuItem = NUM_SUB_MENU_ITEMS - 1;
        if (activeSubMenuItem >= NUM_SUB_MENU_ITEMS) 
            activeSubMenuItem = 0;
    }

    endwin();
    return 0;
}
